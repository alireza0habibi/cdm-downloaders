package com.yadishot.instagramdownloader.UI.Fragments;


import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.snatik.storage.Storage;
import com.yadishot.instagramdownloader.Adapters.AdapterHome;
import com.yadishot.instagramdownloader.R;
import com.yadishot.instagramdownloader.Utils.Tools;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    private static final String CHANNGEL_ID = "1001";
    @BindView(R.id.txt_dot_audio)
    TextView txtDotAudio;
    @BindView(R.id.txt_audo_count)
    TextView txtAudoCount;
    @BindView(R.id.txt_dot_photos)
    TextView txtDotPhotos;
    @BindView(R.id.txt_photo_count)
    TextView txtPhotoCount;
    @BindView(R.id.txt_dot_videos)
    TextView txtDotVideos;
    @BindView(R.id.txt_video_count)
    TextView txtVideoCount;
    @BindView(R.id.Download_now)
    Button DownloadNow;
    @BindView(R.id.latest_files)
    TextView latestFiles;
    @BindView(R.id.recyclerView_latest)
    RecyclerView recyclerViewLatest;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView nav_view;
    @BindView(R.id.nav_layout_home)
    AdvanceDrawerLayout nav_LayoutHome;
    @BindView(R.id.menu_btn)
    ImageButton menuBtn;

    private AdapterHome adapterHome;
    private Storage storage;

    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        nav_view.setNavigationItemSelectedListener(this);
        storage = new Storage(getContext());
        initFile();

        setonClick();
        // init menu item Clickable
        initNavigationDrawer();
        // init item clickable listener
        itemClickable();
    }

    private void itemClickable() {
    }

    private void initNavigationDrawer() {
        nav_LayoutHome.useCustomBehavior(Gravity.START); //assign custom behavior for "Left" drawer
        nav_LayoutHome.useCustomBehavior(Gravity.END); //assign custom behavior for "Right" drawer
    }

    private void setonClick() {
        DownloadNow.setOnClickListener(v -> {
            Tools.fragmentReplaceBackStack(getFragmentManager(), new FragmentDownload());

        });

        menuBtn.setOnClickListener(v -> {
            nav_LayoutHome.openDrawer(GravityCompat.START);
        });
    }

    private void initFile() {
        String path = String.valueOf(Tools.appDirectory());
        recyclerViewLatest.setLayoutManager(new LinearLayoutManager(getContext()));
        List<File> files = storage.getFiles(path);
        adapterHome = new AdapterHome(files, getContext());
        recyclerViewLatest.setAdapter(adapterHome);
    }

    public void onBackPressed() {
        if (nav_LayoutHome.isDrawerOpen(GravityCompat.START)) {
            nav_LayoutHome.closeDrawer(GravityCompat.START);
        } else {
            super.getActivity().onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.power_app:
                Toast.makeText(getContext(), "Hello world", Toast.LENGTH_SHORT).show();
                break;
            case R.id.settings:
                Tools.fragmentReplaceBackStack(getFragmentManager(), new FragmentSettings());
                break;
        }
        return true;
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.navigation_drawer_menu, menu);
        return super.getActivity().onCreateOptionsMenu(menu);
    }

}