package com.yadishot.instagramdownloader.UI.Activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import com.yadishot.instagramdownloader.R;
import com.yadishot.instagramdownloader.UI.Fragments.FragmentHome;
import com.yadishot.instagramdownloader.Utils.Tools;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.frame_container)
    FrameLayout frameContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Tools.fragmentAdd(getSupportFragmentManager(), new FragmentHome());
    }
}
