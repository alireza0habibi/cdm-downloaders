package com.yadishot.instagramdownloader.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.yadishot.instagramdownloader.Models.FilesModel;
import com.yadishot.instagramdownloader.R;
import com.yadishot.instagramdownloader.UI.Activity.MainActivity;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

public class Tools {

    private NotificationManager mNotificationManager;

    // check application folder was created or if not exists mkdirs
    public static File appDirectory() {
        String root = Environment.getExternalStorageDirectory().toString();
        File appDirectory = new File(root + "/CDM");
        if (!appDirectory.exists()){
            appDirectory.mkdirs();
        }
        return appDirectory;
    }

    // Random numbers for the id of each image
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(5);
        int tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (int) (generator.nextInt((999 - 3)) + 3);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }


    // Generate Random name for image and Video

    public static File randomFileNames(int type){
        File existsName = null;
        switch (type){
            case 0:
                String photoName = "IMG_2019_" + random() + ".jpg";
                final File photoFile = new File(photoName);
                existsName = photoFile;
                break;
            case 1:
                String videoName = "VIDEO-" + Tools.random() + ".mp4";
                final File videoFile = new File(videoName);
                existsName = videoFile;
                break;
            case 2:
                String audioName =  Tools.random() + ".mp3";
                final File audioFile = new File(audioName);
                existsName = audioFile;
                break;
        }

        return existsName;
    }


    // fragments init
    public static void fragmentAdd(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().add(R.id.frame_container, fragment).commit();
    }

    public static void fragmentReplace(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    public static void fragmentReplaceBackStack(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
    }


//     get Files
    public static ArrayList<FilesModel> getFiles() {
        ArrayList<FilesModel> galleryModels = new ArrayList<>();

        if (appDirectory().exists()) {

            File[] files = Tools.appDirectory().listFiles();
            FilesModel galleryModelsa;

            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                galleryModelsa = new FilesModel();
                galleryModelsa.setFileName(file.getName());
                galleryModelsa.setFileUri(Uri.fromFile(file));

                // send File Icon switches
                if (file.getName().endsWith(".mp3")){
                    galleryModelsa.setWitch(0);
                }else if (file.getName().endsWith(".jpg")){
                    galleryModelsa.setWitch(1);
                }else if (file.getName().endsWith(".mp3")){
                    galleryModelsa.setWitch(2);
                }
                galleryModels.add(galleryModelsa);

            }
        }

        return galleryModels;
    }

    // get file name on link url
    public static String getFileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        }
        catch(MalformedURLException e) {
            return "";
        }

        int contentIndex = url.indexOf("%");
        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }


//    public static ArrayList<GalleryModels> getVideos() {
//        ArrayList<GalleryModels> galleryModels = new ArrayList<>();
//
//        if (appDirectory().exists()) {
//
//            File[] files = ContentStatic.appDirectory().listFiles();
//            GalleryModels galleryModelsa;
//
//            for (int i = 0; i < files.length; i++) {
//                File file = files[i];
//                if (file.getName().endsWith(".mp4")){
//                    galleryModelsa = new GalleryModels();
//                    galleryModelsa.setImageName(file.getName());
//                    galleryModelsa.setImageUri(Uri.fromFile(file));
//                    galleryModels.add(galleryModelsa);
//                }
//            }
//        }
//
//        return galleryModels;
//    }


    // Notification
    public static void notification(Context context, String ContentTitle, String ContentText){
    }

}
