package com.yadishot.instagramdownloader.Utils;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.yadishot.instagramdownloader.Adapters.AdapterDownloads;
import com.yadishot.instagramdownloader.R;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class DownloaderInit {

    private String url;
    private String path;
    private String fileName;
    private TextView status;
    private Context context;
    private MaterialProgressBar progressBar;
    private FloatingActionButton btnAddDownload;
    private AdapterDownloads adapterDownloads;


    public void DownloaderInits(Context context, String url, String path, String fileName, TextView status, MaterialProgressBar progressBar, FloatingActionButton btnAddDownload, AdapterDownloads adapterDownloads) {
        this.url = url;
        this.path = path;
        this.fileName = fileName;
        this.status = status;
        this.context = context;
        this.progressBar = progressBar;
        this.btnAddDownload = btnAddDownload;
        this.adapterDownloads = adapterDownloads;
        initDownloadeer();
    }

    private void initDownloadeer(){

        int downloadID = PRDownloader.download(url, path, fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        btnAddDownload.setEnabled(false);
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        btnAddDownload.setEnabled(false);

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        progressBar.setVisibility(View.VISIBLE);
                        long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                        progressBar.setProgress((int) progressPercent);
                        status.setText(DownloaderUtils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes));
                        Tools.notification(context, "Downloading", progress.currentBytes +" of "+ progress.totalBytes);
                    }
                }).start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        heyVibrator();
                        adapterDownloads.notifyDataSetChanged();
                        progressBar.setProgress(0);
                        progressBar.setVisibility(View.GONE);
                        btnAddDownload.setEnabled(true);
                    Tools.notification(context, "Downloading Done!", "Download finished!");
                    }

                    @Override
                    public void onError(Error error) {
                        btnAddDownload.setEnabled(true);
                    }
                });
    }

    private void heyVibrator() {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }

}
