package com.yadishot.instagramdownloader.Models;

import android.net.Uri;

public class FilesModel {
    String fileName;
    int witch;
    Uri fileUri;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getWitch() {
        return witch;
    }

    public void setWitch(int witch) {
        this.witch = witch;
    }

    public Uri getFileUri() {
        return fileUri;
    }

    public void setFileUri(Uri fileUri) {
        this.fileUri = fileUri;
    }
}
